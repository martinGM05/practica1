function funciones(){
    $(document).ready(function () {
        console.log("hola")
        let total = parseFloat($('.totalCarrito').text());
        $('.product-image').click(function () {
            if ($(this).css('opacity') == 1) {
                $(this).css('opacity', 0.5);
                $(this).css('border-color', '#005f08');
                $(this).css('background', '#464d49');
                let precio = parseFloat($(this).find('.precio').text());
                total += precio;
                $('.totalCarrito').text(total);
            }else{
                $(this).css('opacity', 1);
                $(this).css('border-color', '#005');
                $(this).css('background', '#fff');
                let precio = parseFloat($(this).find('.precio').text());
                total -= precio;
                $('.totalCarrito').text(total);
            }
        });
    });
}

$.get('https://db-calesa.herokuapp.com/productos', resp => {
    funciones();
    let contador = 1;
    console.log(resp)
    resp.forEach(reps => {
        if(reps.categoria==="hasbro"){
           
            let bandera = 0;
            reps.productos.forEach(producto=>{
                if (bandera===0){
                    console.log(bandera);
                    $("#indexj").append('<article class="article">'+
                    '<div class="aling">'+
                    '<a href="components/seccion'+contador+'.html" class="button button1">Ver más...</a>'+
                    '<h2 id="sc'+contador+'">'+reps.categoria+'</h2>'+
                    '</div>'+
                    '<div class="contenedor" id="q">')   
                    bandera=1;
                    contador++;
                }
                $("#q").append('<div class="product-image">'+
                '<img class="img-producto" src="'+producto.image+'" alt="" />'+
                '<div class="info">'+
                '<h2>'+producto.nombre+'</h2>'+
                '<p>'+producto.descrmini+
                '</p>'+
                '<div class="contentPrecio">'+
                '<p>Precio</p>'+
                '<p class="precio">'+producto.precio+
                '</p>'+
                '</div>'+
                '</div>'+
                '</div>')

            })

            $("#indexj").append('</div>')
            $("#indexj").append('</article>')

        }else if(reps.categoria==="Mattel"){

            let bandera = 0;
            
            reps.productos.forEach(producto=>{
                if (bandera===0){
                    $(".content").append('<article class="article" >'+
                    '<div class="aling">'+
                    '<a href="components/seccion'+contador+'.html" class="button button1">Ver más...</a>'+
                    '<h2 id="sc'+contador+'">'+reps.categoria+'</h2>'+
                    '</div>'+
                    '<div class="contenedor" id="w">')   
                    bandera=1;
                    contador++;
                }
                $("#w").append('<div class="product-image">'+
                '<img class="img-producto" src="'+producto.image+'" alt="" />'+
                '<div class="info">'+
                '<h2>'+producto.nombre+'</h2>'+
                '<p>'+producto.descrmini+
                '</p>'+
                '<div class="contentPrecio">'+
                '<p>Precio</p>'+
                '<p class="precio">'+producto.precio+
                '</p>'+
                '</div>'+
                '</div>'+
                '</div>')

            })

            $("#indexj").append('</div>')
            $("#indexj").append('</article>')

        }else if(reps.categoria==="Fisher Price"){

            let bandera = 0;
            reps.productos.forEach(producto=>{
                if (bandera===0){
                    $(".content").append('<article class="article" >'+
                    '<div class="aling">'+
                    '<a href="components/seccion'+contador+'.html" class="button button1">Ver más...</a>'+
                    '<h2 id="sc'+contador+'">'+reps.categoria+'</h2>'+
                    '</div>'+
                    '<div class="contenedor" id="e">')   
                    bandera=1;
                    contador++;
                }
                $("#e").append('<div class="product-image">'+
                '<img class="img-producto" src="'+producto.image+'" alt="" />'+
                '<div class="info">'+
                '<h2>'+producto.nombre+'</h2>'+
                '<p>'+producto.descrmini+
                '</p>'+
                '<div class="contentPrecio">'+
                '<p>Precio</p>'+
                '<p class="precio">'+producto.precio+
                '</p>'+
                '</div>'+
                '</div>'+
                '</div>')

            })

            $("#indexj").append('</div>')
            $("#indexj").append('</article>')

        }else if(reps.categoria==="Lego"){

            let bandera = 0;
            reps.productos.forEach(producto=>{
                if (bandera===0){
                    $(".content").append('<article class="article" >'+
                    '<div class="aling">'+
                    '<a href="components/seccion'+contador+'.html" class="button button1">Ver más...</a>'+
                    '<h2 id="sc'+contador+'">'+reps.categoria+'</h2>'+
                    '</div>'+
                    '<div class="contenedor" id="r">')   
                    bandera=1;
                    contador++;
                }
                $("#r").append('<div class="product-image">'+
                '<img class="img-producto" src="'+producto.image+'" alt="" />'+
                '<div class="info">'+
                '<h2>'+producto.nombre+'</h2>'+
                '<p>'+producto.descrmini+
                '</p>'+
                '<div class="contentPrecio">'+
                '<p>Precio</p>'+
                '<p class="precio">'+producto.precio+
                '</p>'+
                '</div>'+
                '</div>'+
                '</div>')

            })

            $("#indexj").append('</div>')
            $("#indexj").append('</article>')

        }

        
    });
})
