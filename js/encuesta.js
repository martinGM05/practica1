var expresion = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
// Regex para validar nombres y apellidos
var expresionNombre = /^[a-zA-Z\s]*$/;
// Regex para valida fecha de nacimiento
var expresionFecha = /^(0?[1-9]|[12][0-9]|3[01])[\-](0?[1-9]|1[012])[\-]\d{4}$/;


$(document).ready(function(){
    $('#btnEnviar').click(function(){
        var nombre = $('#formulario_nombre').val();
        var apellido = $('#formulario_apellido').val();
        var sexo = $('#formulario_sexo').val();
        var email = $('#formulario_email').val();
        var fecha = $('#formulario_fecha').val();
        var interes = $('#formulario_interes:checked').val();
        if(nombre == ""){
            alert("El campo Nombre no puede estar vacio");
            return false;
        }
        if(apellido == ""){
            alert("El campo Apellido no puede estar vacio");
            return false;
        }
        if(sexo == ""){
            alert("El campo Sexo no puede estar vacio");
            return false;
        }
        if(email == ""){
            alert("El campo Email no puede estar vacio");
            return false;
        }
        if(!expresion.test(email)){
            alert("El email no tiene un formato válido");
            return false;
        }
        if(!expresionNombre.test(nombre)){
            alert("El campo Nombre no tiene un formato válido");
            return false;
        }
        if(!expresionNombre.test(apellido)){
            alert("El campo Apellido no tiene un formato válido");
            return false;
        }
        if(fecha == ""){
            alert("El campo Fecha no puede estar vacio");
            return false;
        }
        if(!expresionFecha.test(fecha)){
            alert("El campo Fecha no tiene un formato válido");
            return false;
        }
        if(!interes){
            alert("El campo Interes no puede estar vacio");
            return false;
        }
        // Funcion que me regrese los valores en un alert
        alert("Nombre: " + nombre + "\nApellido: " + apellido + "\nSexo: " + sexo + "\nEmail: " + email + "\nFecha: " + fecha + "\nInteres: " + interes);  
    });
    // Mensaje de confirmacion con opciones
    $('#btnCancelar').click(function(){
        var respuesta = confirm("¿Desea volver a la página anterior?");
        if(respuesta){
            window.location.href = "../index.html";
        }else{
            return false;
        }
    });
    // Restablecer valores
    $('#btnRestablecer').click(function(){
        $('#formulario_nombre').val("");
        $('#formulario_apellido').val("");
        $('#formulario_sexo').val("");
        $('#formulario_email').val("");
        $('#formulario_fecha').val("");
        $('#formulario_interes:checked').prop('checked', false);
    });

});
